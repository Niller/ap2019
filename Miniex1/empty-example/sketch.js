// draw a sphere with radius 200
function setup() {
  createCanvas(1200,700, WEBGL);
background('rgba(0,255,0, 0.25)');
}

function draw() {
  let c = color(255, 204, 0); // Define color 'c'
  fill(c);
background(200);
background('rgba(0,255,0, 0.25)');
noStroke();

rotateZ(frameCount * 0.02);
push();
fill(210,0,0)
    rotateX(frameCount * 0.02);

  sphere(50);

push();
fill(210,200,0)
  rotateX(frameCount * 0.01);
  //rotateZ(frameCount * 0.01);
  translate(0,120,200)
sphere(70);

fill(0,200,0)
  rotateX(frameCount * 0.01);
  //rotateZ(frameCount * 0.01);
  translate(1,1,1)
sphere(70);
pop();
pop();





}
