miniex1 - https://cdn.staticaly.com/gl/Niller/ap2019/raw/master/Miniex1/empty-example/index.html 

![screenshot](Skærmbillede.png)
Miniex1

Describe your first independent coding process 

This is my miniex1. It has been a very interesting process for me. At the beginning I was very frustrated, because I didn’t understood how it was working. I found it very helpful, when we got the instruction at Wednesday. Then we had got a little break from it, and you have the opportunity to get a new and fresh start, but with the information you have learned previously! Slowly, I have learned more and more, and now I am very excited that I have learned to work in 3D. It took me very long time (with the help of Ann) to learn how to get the ball circling around the ball in the middle. But I now understand, that it is because of the x, y and z. When you have to get the ball around the other one, you don’t turn the ball around, but actually the hole canvas itself to rotate. 



How your coding process is differ or similar to reading and writing text? 

I can’t see how the coding process is similar to writing text, other then to write to the right, and you read down the page. But I can imagine when you become better, you would maybe understand just like writing. But right now I am not good enough to understand it like writing a text. 



What is code and coding/programming practice means to you?

I find it a bit nerdy, but at the same time very nice, that I finally have the opportunity to become a nerd myself!  Right now I also means a lot of frustration. But I am looking so much forward to become a pro at programming! 

