![ScreenShot](Skærmbillede1.png)

[RUNME](https://cdn.staticaly.com/gl/Nicolaibb/ap2019/raw/master/Miniex7/p5/empty-example/index.html)



In group 7 we have created a program, that has the purpose of illustrating how surveillance has spread worldwide. We think that surveillance is a part of the human nature and has always been a factor of the humans. The oldest fossils of the Homo Sapiens are believed to be about 200,000 or perhaps 300,000 years old and back then we started to originate from Africa. 
In the daily living back then, they also used surveillance for hunting animals and enemy tribes that would maybe hurt them. Basically they used it for survival. Naturally, they didn’t have cameras and modern technology, so they used their eyes and ears instead. Technology, is now so good, that we can monitor other people’s life from other space. It is easy and it is all over the world. Surveillance can both be good and bad, but the essential is that we use it all the time. Therfor is our program programmed to start in Afrika (just like the humans did), and spreaded all over the world. The green rectangles illustrates the surveillance, which is happening at the moment. We also set ourselves some rules:  

Rule 1: Set the first rectangle in Africa. The program has to start somewhere and we made a rule which has the purpose to start the program in the same way every single time.

Rule 2: Generate a random size, x position and y position for the new rectangle. The program generate new rectangles all the time and to do that we made a rule which decided the position and size of it.

Rule 3: Check which rectangle is the closest to the current rectangle. Every time the program generates a new rectangle it is always 'in line' or very close to the one genereted just before. This is also done by one of our rules and it makes the rects generated in a way that dosn't look like it is random.

Rule 4: Show the original position of the rectangle in the shape of a circle and create a line to the new position that represent the angle. As you can see in the program there is a lot of lines where all of them have a circle at the end, but they only shows for a short moment. This happens every time that a new rect is generated. The line shows from where the old rect was and in which way the new one will be generated.

Rule 5: Align the new position to the closest rectangle outline.

Rule 6: Give the rectangle a random color within the green spectrum. No rect is similair to eachother in this program and this is also valid in relation to color. Every rect has a new color, but within the limits of the green spectrum.

Rule 7: Repeat until the max count of the rectangles is reached. The program actually has a stopping point. When it has created 5000 rects it will stop. Both because it won't be visible anymore because it is out of the screen, but also to avoid a crash  



