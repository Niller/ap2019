
let img

function setup() {

img= loadImage('face.PNG');
  var c = createCanvas(windowWidth, windowHeight);
  c.position(0,75); //making so that the webcam could be shown on the canvas
//creating the button that follows the mouse
button= createButton ('PUSH TO SEE YOUR MOOD');
button.position(500,300);
button.mousePressed(webcam);
capture= createCapture(VIDEO);
capture.hide();
background(220,217,216);
//data button

}
function draw () {

fill(10);
textSize (10);
text ('Remeber to update your mood',50,350);
text('Last update: 10sek.',50,370);
//Text above slilder
textSize (25);

text('Current mood?',660,150);
//Creating a smiley
//face
noFill();
circle (730,250,50,20);
//eyes
fill(0);
circle(705,240,5);
circle(755,240,5);
//mouth
noFill();


pop();
//happy mouth
push();
noFill();
if(slidervalue>170){
  arc(730, 250, 80, 80, 0, PI);
}else{
  stroke(220,217,216)
    arc(730, 250, 80, 80, 0, PI);
}
pop();
//Getting button to follow the mouse
button.position(mouseX,mouseY+75);
//Blue facebook color above
fill(19,93,217);
rect(0,0,1525,95);
//rectangle with information
push();
strokeWeight(2);
noFill();
rect(25,100,360,600);
pop();
}
//information and function of the button 'likes and dislikes'
function likes() {
fill(0);
textSize(20);
text('You like:',1200,450);
text('You dislike:', 1300,450);
//likes
textSize(15);
text('Food',1200,470);
text('Dogs',1200,490);
text('Music',1200,510);
text('Walks',1200,530);
text('Instagram',1200,550);
text('Netflix',1200,570);
text('Working out',1200,590);
//dislikes
text('People that chew loudly',1300,470);
text('Vomit',1300,490);
text('Public transportation',1300,510);
text('Shopping for shoes',1300,530);
text('Advertisements',1300,550);
text('Snakes',1300,570);
text('Tequila',1300,590);
}
//Webcam
function webcam (){
capture.position(500, 300);
capture.show(); //getting the webcam to show when you push the button
}
//information in the buttin 'click to see your personal data'
