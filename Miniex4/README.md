![ScreenShot](skærmbillede1.png)

![ScreenShot](skærmbillede2.png)

[RUNME](https://cdn.staticaly.com/gl/Niller/ap2019/raw/master/Miniex4/empty-example/index.html)

Describe about your sketch and explains what have been captured both conceptually and technically. How's the process of capturing?

My sketch  is catching your current mood. Conceptually is meant to express the way your always try to show your “better-you” on social media. I think one of the big problems about the increasing digitalizing of the world is that you always try to show the best side of yourself and never when you are sad or in a bad mood. This is creating a false sense of life itself. Life is never just a happy ride, but on the social media it can sometimes seems like that. I know the theme CAPTURE is about how the Facebook and the internet in general are capturing your data, but I think it’s interesting how they are maybe capturing false data, because people always try to show their “better-self”. The majority of the time spent in front of your computer will not be with a smile on your face, but instead just a regular face or even a “ugly” face(a face your for sure wouldn’t expound too your social media accounts.). Looking at Instagram and the comments on the pictures is always a lot of acknowledgement, which also creates a lot of pressure on people. 

Together with the assigned reading and coding process, how might this ex helps you to think about or understand the data capturing process in digital culture?

I think the assigned reading and making my own program helped me see how much information the web could know about me. I was already aware that many things were being captured but I was not aware of the scale. I thinks it is very disturbing how much is being captured. One of the things I found incredibly interesting and which I have thought a lot about subsequently, is the reason why they choose to do so. I think much of it is due to the money and how they can make even more people use their platforms and make it the most attractive of them all. In the assigned reading they say that:  

“We link Facebook’s efforts to a historical perspective on the so-called hit and link economy. (..) Doing so, we claim that what is in the making, is not only a social web, but also a recentralised, data-intensive infrastructure which we conceptualise as a ‘Like economy’.”

Due to the fact that people use facebook's "like" button they also create a data collection where they can see what people want and don’t want. 


