
let s = "Hold any key"
let b="anxiety"
let a="happy"

function setup() {

  createCanvas(800,500)
  background(0);

}


function draw() {

noStroke()

   fill(255,105,180);
   text(s, 310, 200, 300, 80);

   fill(0,100,0);
   text(b, 183, 380, 300, 80);

   fill(150,0,0);
   text(a, 480, 380, 300, 80);

push();

//SMILEY NR 1
fill(255,204,50);
ellipse(200, 250, 200, 210);

//højre øje bagved
fill(244,244,244);
ellipse(230, 235, 50, 60);

//venstre øje bagved
noStroke()
fill(244,244,244);
ellipse(166, 235, 50, 60);

//venstre øje
fill(0,0,0);
ellipse(173, 245, 25, 25);


//højre øje
fill(0,0,0);
ellipse(238, 245, 25, 25);

//mund venstre
fill(235)
arc(205, 300, 70, 10, -1,4, PI + QUARTER_PI, CHORD);


//SMILEY NR 2
fill(255,204,51);
ellipse(500, 250, 200, 210);

//behind right eye
fill(244,244,244);
ellipse(540, 233, 50, 50);

//behind left eye
fill(244,244,244);
ellipse(460, 233, 50, 50);

//left eye
fill(0);
ellipse(454, 241, 20, 20);

//right eye
fill(0);
ellipse(534, 242, 20, 20);


//mouth right
fill(30)
arc(500, 300, 60, 50, -0.7, PI + QUARTER_PI, CHORD);

pop();


if (keyIsPressed === true) {
    noFill()
    noStroke();

//SMILEY 1

//højre øje bagved
fill(244,244,244);
ellipse(230, 220, 50, 100);

//venstre øje bagved
noStroke()
fill(244,244,244);
ellipse(166, 220, 50, 100);

//venstre øje
fill(0,0,0);
ellipse(173, 245, 10, 10);


//højre øje
fill(0,0,0);
ellipse(238, 245, 10, 10);

//mund venstre
fill(235)
arc(205, 300, 70, 50, -1,4, PI + QUARTER_PI, CHORD);

//SMILEY 2 with tung

    //behind right eye
    fill(244,244,244);
    ellipse(540, 220, 60, 70);

    //behind left eye
    fill(244,244,244);
    ellipse(460, 220, 60, 70);

    //left eye
    fill(0);
    ellipse(458, 228, 35, 35);

    //right eye
    fill(0);
    ellipse(538, 228, 35, 35);

    //mouth right
    fill(30)
    arc(500, 300, 80, 80, -0.7, PI + QUARTER_PI, CHORD);

    //tunge
    fill(255,105,180)
    arc(500, 315, 55, 70, -0.7, PI + QUARTER_PI, CHORD);

}

}
