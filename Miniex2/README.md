![ScreenShot](Skærmbillede 2.png)


[RUNME](https://cdn.staticaly.com/gl/Niller/ap2019/raw/master/Miniex2/empty-example/index.html)

I have made two different Emojiis. One of them has the tongue out, which took me some time. It doesn’t do anything. I have tried to play around with the arc(), which was very funny. I’m not sure that I got it completely and I understand the syntax in that one, but I’ve made something.  

I’m slowly starting to understand, that it works just like photoshop with “layers”, and you therefor have to have something apove other things and something under some other stuff. 


What I learnt from the assigned reading, was that people felt shocked that there were only Emojiis with some type of skin color, only male or female genders and the stereotype family (mom, dad and two children). It only seems okay, if the Emojiis are completely neutral, and people therefore don’t fell encountered for it. 
