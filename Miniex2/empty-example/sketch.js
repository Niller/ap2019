

function setup() {

  createCanvas(800,500)
  background(0,0,0)
}

function draw() {


//smiley nr. 1
fill(255,204,50);
ellipse(200, 250, 200, 210);

//venstre

//venstre øje bagved
fill(244,244,244);
ellipse(166, 220, 50, 100);

//venstre øje
fill(0,0,0);
ellipse(175, 245, 25, 25);

//højre

//højre øje bagved
fill(244,244,244);
ellipse(230, 220, 50, 100);

//højre øje
fill(0,0,0);
ellipse(239, 245, 25, 25);


//smiley nr. 2
fill(255,204,51);
ellipse(500, 250, 200, 210);

//højre øje bagved
fill(244,244,244);
ellipse(460, 220, 50, 70);

//højre øje
fill(0,0,0);
ellipse(458, 241, 25, 25);

//højre øje bagved
fill(244,244,244);
ellipse(540, 220, 50, 70);

//højre øje
fill(0,0,0);
ellipse(538, 242, 25, 25);

//mund venstre
fill(244,244,244)
arc(210, 300, 80, 80, -0.7, PI + QUARTER_PI, CHORD);

//mund højre
fill(0,0,0)
arc(500, 320, 80, 80, -0.7, PI + QUARTER_PI, CHORD);

//tunge
fill(255,105,180)
arc(500, 350, 60, 100, -0.7, PI + QUARTER_PI, CHORD);
}
