![ScreenShot](Skærmbillede1.png)

[RUNME](https://cdn.staticaly.com/gl/Niller/ap2019/raw/master/Miniex6/empty-example/index.html)

Describe how does your game/game object work?

My game is similar to the game called Flappy Bird, which was very popular some years ago. It works by pressing SPACEBAR down so that the bird is able to fly. In front of you, there will be some challenges in the form of piles to try to get through and if you hit them the game will be over. It has been very difficult to program such that the level is neither too slight nor too easy. Here are some parameters you can move on: 
If the poles become too wide and the width between them becomes too close, the game becomes very difficult. Conversely, the game is very easy if the poles are far away from each other and each one is very narrow. In addition, you can also do something about the bird that affects the level of difficulty. If the bird can either rise too high or fall too low at the touch of SPACEBAR, it becomes difficult aswell and vice versa it will be easy if you have easy control over it.

Describe how you program the objects and their related attributes and methods in your game.

I used the class function to organize my code. This was my first time I tried that, and it was really nice. I think it is helping a lot to organizing the code, and it also makes it a lot easier to program I think. At first I was thinking it would be even more confussing not to have it in the same program, but actually it was making it much easier just to callback the class you have made. 
