



function setup() {

createCanvas(windowWidth,windowHeight);
frameRate(12);


}

function draw() {

  push();
  fill(40,190,148);
  rect(0,0,width,height);
  drawThrobber(20);
  pop();

}

function drawThrobber(num) {
  push();
  translate(width/2,height/2);
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();
  fill(220,220,220);
  circle(30, 30, 20);
  pop();

  


}
